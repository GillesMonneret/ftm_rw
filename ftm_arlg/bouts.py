import pandas as pd
import numpy as np

def enrich_with_bouts(df_strides, use_side=True, mrt=3, name_col="boutId"):
	df_enriched = df_strides.sort_values("timeHeelStrike2")
	df_enriched["delta_t"] = df_enriched.\
	groupby(["recordId"]).transform(lambda x: x-x.shift(1))["timeHeelStrike2"]/1000
	df_enriched["break_time"] = (df_enriched["delta_t"]>mrt)*1
	if use_side:
		df_enriched["break_side"] = (df_enriched["side"]==(df_enriched["side"].shift(-1)))*1
		df_enriched["break_group"] = df_enriched[["break_time","break_side"]].fillna(0).max(axis=1)
		df_enriched[name_col] = df_enriched.groupby(["recordId","side"])["break_group"].\
		transform(lambda x: x.cumsum())
	else:
		df_enriched["break_group"] = df_enriched["break_time"]
		df_enriched[name_col] = df_enriched.groupby(["recordId"])["break_group"].\
		transform(lambda x: x.cumsum())
	return(df_enriched)
