import pandas as pd
import numpy as np
import seaborn as sns

def create_duchenne_sd(df_strides):
    time_per_patient = df_strides.groupby(["obsId","boutId","side"])["timeHeelStrike2"].\
                   agg(lambda x: x.max()-x.min()).dt.total_seconds().reset_index()
    time_per_patient = time_per_patient.rename(columns = {"timeHeelStrike2":"boutDuration"})
    time_per_patient["cum"] = time_per_patient.groupby(["obsId","side"])["boutDuration"].transform(lambda x: x.cumsum())
    time_per_patient["total"] = time_per_patient.groupby(["obsId","side"])["cum"].transform(lambda x: x.max())

    return time_per_patient
    
    
def compute_95velSD_boutActivity(time_per_patient, df_strides, cut_off, window, group_max, only_first, kind="minutes"):
    if kind=="minutes":
        kind_time = 60 # for minutes
    elif kind=="hours":
        kind_time = 3600 # for hours
    ## window in minutes
    df_total_activities = time_per_patient[time_per_patient.side=="left"].groupby(["obsId"])["total"].first().sort_values()/kind_time
    focus_patients = df_total_activities[df_total_activities>cut_off].index.to_list()
    df_strides = df_strides[df_strides.obsId.isin(focus_patients)]
    
    ## Create subgroup
    time_per_patient["group_duchenne"] = time_per_patient["cum"]//(kind_time*window)
    time_per_patient["group_duchenne"] = time_per_patient["group_duchenne"].astype(int)
    df_strides_duchenne = df_strides[["obsId","boutId","side","velocity"]].\
        merge(time_per_patient[["obsId","boutId","side","group_duchenne"]],
        on=["obsId","boutId","side"])

    if only_first : 
        df_strides_duchenne = df_strides_duchenne[df_strides_duchenne.group_duchenne<group_max]

    ## We only look left side because it's easier to take account of this activity
    df_strides_duchenne = df_strides_duchenne[df_strides_duchenne.side=="left"]

    ## Compute 95-vel for each group
    df_95pc = df_strides_duchenne.groupby(["obsId","group_duchenne"])["velocity"].\
          agg(lambda x: x.quantile(0.95)).reset_index()
    
    ## EMA take the variability of the difference 
    df_95_duchenne = df_95pc.set_index("obsId").\
                 groupby("obsId").transform(lambda x: x-x.shift(-1)).dropna().reset_index()
    df_95_duchenne = df_95_duchenne.groupby(["obsId"]).agg([np.mean, np.std]).reset_index()
    df_true_mean = df_95pc.groupby("obsId").mean().reset_index().rename(columns={"velocity":"true_mean"})
    df_95_duchenne = df_95_duchenne.merge(df_true_mean, on=["obsId"])
    
    df_95_duchenne.columns = [el[0]+'_'+el[1] if type(el)==tuple else el for el in df_95_duchenne.columns]
    df_95_duchenne["percent"] = df_95_duchenne["velocity_std"]/df_95_duchenne["true_mean"]
    
    return df_95_duchenne
    
def visualization(df_duchenne, dataset_bouts, cut_off, window_sup, kind):

    """
    This function calculates the Duchenne variability for a window from 1 to window_sup and then plots the results 
    cut_off : minimum number of hours per patient

    """

    list_series = []
    for i in range(1,window_sup):
        result = compute_95velSD_boutActivity(df_duchenne, dataset_bouts,
                                              window=i, cut_off=cut_off, group_max=6,
                                              only_first = False, kind="minutes")
        result = result.set_index(["obsId"])
        list_series.append(result["percent"])
    result_activityFiles = pd.concat(list_series, axis=1)
    result_activityFiles.columns = [i for i in range(1,window_sup)]

    df_to_plot = result_activityFiles.transpose().reset_index()
    df_to_plot = pd.melt(df_to_plot, id_vars="index",
                     value_vars=df_to_plot.columns.tolist()[1:], value_name="percent")
    df_to_plot = df_to_plot.rename(columns={"index":"window_hour"})
    sns.set_style('ticks')
    fig, ax = plt.subplots()
    fig.set_size_inches(11.7, 8.27)
    fig = sns.lineplot(data=df_to_plot, x="window_hour", y="percent", hue="obsId", legend = False)
    fig = sns.lineplot(data=df_to_plot, x="window_hour", y="percent", color = 'blue')
    plt.axhline(y = 0.05, linestyle = '--')
    return df_to_plot
