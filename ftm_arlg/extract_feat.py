import pandas as pd
import numpy as np

def enrich_with_assym(df_strides):
	colBase = ["stanceDuration", "swingDuration"]
	colAssym = [el+"Assym" for el in colBase]
	## Take long because we have many many bouts
	## Faster, but more memory?
	for col in colBase:
		df_strides[col+"_lag"] = df_strides.groupby(["recordId","boutId"])[col].shift(1)
		df_strides[col+"Assym"] = 0.5*(df_strides[col]-df_strides[col+"_lag"])/\
		(df_strides[col]+df_strides[col+"_lag"])
	df_strides[colAssym] = df_strides.groupby(["recordId","boutId"])[colBase].\
	transform(lambda x: 0.5*(x-x.shift(1))/(x+x.shift(1)))
	df_strides["nanOdd"] = df_strides.groupby(["recordId","boutId"])["side"].\
	transform(lambda x: (x=="left")&(x.shift(1)=="right"))
	df_strides["nanOdd"] = np.where(df_strides["nanOdd"], 1, np.nan)
	for col in colAssym:
		df_strides[col] = df_strides["nanOdd"]*df_strides[col]
	return df_strides
	
def agg_features_bouts(df_strides):
	df_agg = df_strides.groupby(["recordId","boutId","side"])
	## Bouts length
	df_boutLength = df_agg["strideDuration"].agg("count")
	df_boutLength.name = "boutLength"
	## Bouts Duration
	df_strideBouts = df_agg["strideDuration"].agg(lambda x: x[1:].sum())
	df_firstSwing = df_agg["swingDuration"].agg("first")
	df_durationBouts = df_strideBouts + df_firstSwing
	df_durationBouts.name = "boutDuration"
	## Bouts distance
	df_distanceBouts = df_agg["strideLength"].agg("sum")
	df_distanceBouts.name ="boutDistance"
	df_aggs = pd.concat([df_boutLength, df_durationBouts, df_distanceBouts], axis=1)
	return df_aggs.reset_index()
	
def aggTime(df_realWorld):
	df_time = df_realWorld[["PID", "visit_name", "timeToeOff", "strideLength", "strideDuration"]].copy()
	df_time = df_time.drop_duplicates(subset=["PID", "visit_name", "timeToeOff"])
	df_time["dayOfYear"] = df_time["timeToeOff"].dt.dayofyear
	df_time["dailyTime"] = df_time.groupby(["PID", "visit_name", "dayOfYear"])["timeToeOff"].\
	transform(lambda x: x.max()-x.min())
	df_time["dailySteps"] = df_time.groupby(["PID", "visit_name", "dayOfYear"])["timeToeOff"].\
	transform(lambda x: x.count())
	## Best 6MWT
	rollDistance = df_time.set_index("timeToeOff").groupby(["PID", "visit_name", "dayOfYear"])\
	["strideLength"].rolling("6min").sum().reset_index()
	rollDistance = rollDistance.rename(columns={"strideLength": "rollingDaily6MWT"})
	rollTime = df_time.set_index("timeToeOff").groupby(["PID", "visit_name", "dayOfYear"])\
	["strideDuration"].rolling("6min").sum().reset_index()
	rollTime = rollTime.rename(columns={"strideDuration": "rollingDaily6MWT_duration"})
	df_time = df_time.merge(rollDistance,
	on=["PID", "visit_name", "dayOfYear", "timeToeOff"],
	how="outer")
	df_time = df_time.merge(rollTime,
	on=["PID", "visit_name", "dayOfYear", "timeToeOff"],
	how="outer")
	df_time["bestDaily6MWT"] = df_time.groupby(["PID", "visit_name", "dayOfYear"])["rollingDaily6MWT"].\
	transform(lambda x: x.max())/2
	# Wrong quantity, i should take the index and use it
	df_time["bestDaily6MWTTime"] = df_time.groupby(["PID", "visit_name", "dayOfYear"])
	["rollingDaily6MWT_duration"].\
	transform(lambda x: x.max())/120000
	## Agg per status
	col_time = ["PID", "visit_name", "dayOfYear", "dailyTime", "dailySteps",
	"bestDaily6MWT", "bestDaily6MWTTime"]
	df_time = df_time[col_time].\
	drop_duplicates().sort_values("dayOfYear")
	df_time["deltaDay"] = df_time.groupby(["PID", "visit_name"])["dayOfYear"].transform(lambda x: x-x.shift(1))
	df_time["deltaDay"] = df_time["deltaDay"].fillna(0).astype(int)!=1
	df_time["idConsec"] = df_time.groupby(["PID", "visit_name"])["deltaDay"].transform(lambda x: np.cumsum(x))
	df_time["consecutiveDays"] = df_time.groupby(["PID", "visit_name", "idConsec"])["deltaDay"].\
	transform(lambda x: x.count())
	df_agg_time1 = df_time.groupby(["PID", "visit_name"], as_index=False)["consecutiveDays"].max()
	df_agg_time2 = df_time.groupby(["PID", "visit_name"], as_index=False)["dayOfYear"].nunique()
	df_agg_time2 = df_agg_time2.rename(columns={"dayOfYear": "uniqueDays"})
	df_agg_time3 = df_time.groupby(["PID", "visit_name"], as_index=False)["dailyTime"].\
	agg(["sum", "max"])
	df_agg_time3 = df_agg_time3.rename(columns={"sum":"totalTime", "max":"maxDailyTime"})
	df_agg_time4 = df_time.groupby(["PID", "visit_name"], as_index=False)["dailySteps"].\
	agg(["sum", "mean", "std", "max"])
	df_agg_time4 = df_agg_time4.rename(columns={"sum":"totalSteps",
	"mean":"meanDailySteps",
	"std": "stdDailySteps",
	"max":"maxDailySteps"})
	df_agg_time5 = df_time.groupby(["PID", "visit_name"], as_index=False)[["bestDaily6MWT",
	"bestDaily6MWTTime"]].max()
	df_agg_time5 = df_agg_time5.rename(columns={"bestDaily6MWT":"best6MWT",
	"best6MWTTime":"bestDaily6MWTTime"})
	
	df_agg_time = df_agg_time1.merge(df_agg_time2, on=["PID","visit_name"]).\
	merge(df_agg_time3, on=["PID","visit_name"]).\
	merge(df_agg_time4, on=["PID","visit_name"]).\
	merge(df_agg_time5, on=["PID","visit_name"])
	df_agg_time["meanDailyTime"] = df_agg_time["totalTime"].dt.total_seconds()/df_agg_time["uniqueDays"]
	df_agg_time["meanDailyTime"] = pd.to_timedelta(df_agg_time["meanDailyTime"], unit="s")
	for col in ["totalTime","maxDailyTime","meanDailyTime"]:
		df_agg_time[col] = df_agg_time[col].dt.total_seconds()/60
	return df_agg_time
