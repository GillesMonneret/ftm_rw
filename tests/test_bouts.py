import pandas as pd
import numpy as np
from google.cloud import bigquery
client = bigquery.Client()

# from ftm_arlg.bouts import enrich_with_bouts

def enrich_with_bouts(df_strides, use_side=True, mrt=3, name_col="boutId"):
	df_enriched = df_strides.sort_values("timeHeelStrike2")
	df_enriched["delta_t"] = df_enriched.\
	groupby(["recordId"]).transform(lambda x: x-x.shift(1))["timeHeelStrike2"]/1000
	df_enriched["break_time"] = (df_enriched["delta_t"]>mrt)*1
	if use_side:
		df_enriched["break_side"] = (df_enriched["side"]==(df_enriched["side"].shift(-1)))*1
		df_enriched["break_group"] = df_enriched[["break_time","break_side"]].fillna(0).max(axis=1)
		df_enriched[name_col] = df_enriched.groupby(["recordId","side"])["break_group"].\
		transform(lambda x: x.cumsum())
	else:
		df_enriched["break_group"] = df_enriched["break_time"]
		df_enriched[name_col] = df_enriched.groupby(["recordId"])["break_group"].\
		transform(lambda x: x.cumsum())
	return(df_enriched)

def test_bouts():
	query = """
	SELECT * FROM insole-dashboard-dev.ftm_gaitnpark.feat_realWorld
	WHERE recordId='4seE9gx4DzKM3KS9iSQu'
	"""
	df = client.query(query).\
	     to_dataframe().drop_duplicates()
        
	df = df.sort_values("timeHeelStrike2")
	df_enriched = enrich_with_bouts(df, use_side=False,
					 mrt=6, name_col="boutId_test")
	assert all([a == b for a, b in zip(df.boutId.tolist(),
					    df_enriched.boutId_test.tolist())])
